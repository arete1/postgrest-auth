-- Deploy aprt:data_schema to pg

BEGIN;

drop schema if exists data cascade;
create schema data;
set search_path = data, public;

-- create the type specifying the types of users we have (this is an enum).
-- you most certainly will have to redefine this type for your application
create type user_role as enum ('webuser');

-- create the default table definition for the user model used by the auth lib
-- you can choose to define the "user" table yourself if you need additional columns
select settings.set('auth.data-schema', current_schema);
create table "user" (
	id                   serial primary key,
	email                text not null unique,
	"password"           text not null,
	"role"				 user_role not null default settings.get('auth.default-role')::user_role,

	check (email ~* '^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$')
);

create trigger user_encrypt_pass_trigger
before insert or update on "user"
for each row
execute procedure auth.encrypt_pass();


COMMIT;
