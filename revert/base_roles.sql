-- Revert aprt:base_roles from pg
\set authenticator authenticator
\set anonymous anonymous

BEGIN;

drop owned by :authenticator;
drop role if exists :authenticator;

drop owned by :anonymous;
drop role if exists :anonymous;

COMMIT;
